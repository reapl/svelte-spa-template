module.exports = {
  env: {
    es6: true,
    commonjs: true
  },
  plugins: ["prettier", "svelte3"],
  parserOptions: {
    ecmaVersion: 2019,
    sourceType: "module"
  },
  rules: {
    "camelcase": "warn",
    "radix": "warn",
    "array-bracket-spacing": [
      "error",
      "never",
      {
        objectsInArrays: false
      }
    ],
    "object-curly-spacing": [
      "error",
      "never",
      {
        arraysInObjects: false
      }
    ],
    "consistent-return": 2,
    "indent": [
      "warn",
      2,
      {
        SwitchCase: 1,
        ArrayExpression: 1,
        ObjectExpression: 1
      }
    ],
    "linebreak-style": 0,
    "quotes": ["error", "double"],
    "semi": ["error", "always"],
    "switch-colon-spacing": [
      "error",
      {
        after: true,
        before: false
      }
    ],
    "space-before-function-paren": [
      "error",
      {
        anonymous: "always",
        named: "always",
        asyncArrow: "always"
      }
    ]
  }
};
