import alias          from "@rollup/plugin-alias";
import svelte         from "rollup-plugin-svelte";
import resolve        from "rollup-plugin-node-resolve";
import commonjs       from "rollup-plugin-commonjs";
import livereload     from "rollup-plugin-livereload";
import postcss        from "rollup-plugin-postcss";
import autoPreprocess from "svelte-preprocess";
import {terser}       from "rollup-plugin-terser";
import copy           from "rollup-plugin-copy-assets";
import cleanup        from "rollup-plugin-cleanup";
import {generateSW, injectManifest}   from "rollup-plugin-workbox";
import {workboxConfig, workboxInject} from "./workbox-config.js";
//------------------------------------------------------------------------------
const production = !process.env.ROLLUP_WATCH;
//------------------------------------------------------------------------------
export default {
  input: "src/main.js",
  output: {
    sourcemap: !0,
    format: "iife",
    name: "app",
    file: "public/bundle.js"
  },
  plugins: [
    alias({
      entries: {
        "@assets":"src/assets",
        "@cmpnts": "src/components",
        "@pages": "src/pages",
        "@utils": "src/utils",
      }
    }),
    postcss({
      extract: "public/material.css",
      minimize: production,
      use: [
        ["sass", {
          includePaths: [
            "./src/styles",
            "./node_modules",
          ]
        }]
      ],
    }),
    svelte({
      preprocess: autoPreprocess({postcss: true}),
      dev: !production,
      css: css => css.write("public/bundle.css"),
      //! The emnitCss portion was the one the created the problem of other
      //! "global" CSS files being written with Svelte CSSes.
      emitCss: !!0
    }),
    resolve({
      browser: true,
      dedupe: importee =>
        importee === "svelte" || 
        importee.startsWith("svelte/")
    }),
    copy({
      assets: ["src/assets"]
    }),
    commonjs(),
    generateSW(workboxConfig),
    // injectManifest(workboxInject),
    //! Removing all comments including licensing comments as it creates bloat
    //! to the overall application size.
    cleanup({comments: "none"}),
    !production && livereload("public"),
    production && terser()
  ],
  watch: {clearScreen: !0}
};
