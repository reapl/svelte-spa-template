/* eslint-disable no-undef */
import App from "@pages/App/App.svelte";
import registerSW from "@utils/RegisterServiceWorker.js";
registerSW();
const app = new App({target: document.body, props: {}});
export default app;