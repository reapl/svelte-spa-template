import mapbox from "mapbox-gl";
// access token to be kept confidential in the future. now just leave it.
const MAPBOX_ACCESS_TOKEN = "pk.eyJ1IjoiaGVucnlvbmciLCJhIjoiY2s0aTRhYmtnMWd4OTNtcWRqcnd0dzU5dCJ9.eRALun4kX--60Fj4FdHUKg";
mapbox.accessToken = MAPBOX_ACCESS_TOKEN;

const key = {};
export {mapbox, key};