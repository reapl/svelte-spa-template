export default function () {
  if ("serviceWorker" in navigator) {
    //* Register the service worker.
    window.addEventListener("load", function () {
      navigator
        .serviceWorker
        .register("/sw.js")
        .then(() => {
          /** 
           ** Once registration is successful, there are a few choices to make.
           ** You may want to add some analytics about the user upon installation
           ** of the app!
           ** In this case, just logging successful will do.
           */
          console.log("Successfully registered Service Worker.");
        })
        .catch(() => console.error("Service Worker registration failed."));
    });
  }
}