import {writable} from "svelte/store";

const burgerMenu = writable(false);
export {burgerMenu as default};
