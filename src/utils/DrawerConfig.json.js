const DRAWER_LIST = [
  {
    href: "javascript:void(0)",
    activationType: "Map",
    materialIcon: "map",
    itemText: "Map"
  },
  {
    href: "javascript:void(0)",
    activationType: "Commute",
    materialIcon: "commute",
    itemText: "Commute"
  },
  {
    href: "javascript:void(0)",
    activationType: "Train",
    materialIcon: "train",
    itemText: "SMRT Map"
  },
  {
    href: "javascript:void(0)",
    activationType: "Drafts",
    materialIcon: "drafts",
    itemText: "Drafts"
  },
  {
    itemText: "seperator"
  },
  {
    href: "javascript:void(0)",
    activationType: "Settings",
    materialIcon: "settings",
    itemText: "Settings"
  },
  {
    href: "javascript:void(0)",
    activationType: "Friends",
    materialIcon: "bookmark",
    itemText: "Friends"
  },
  {
    href: "javascript:void(0)",
    activationType: "Workbook",
    materialIcon: "bookmark",
    itemText: "Workbook"
  }
];

export {DRAWER_LIST as default};