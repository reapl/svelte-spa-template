import {writable} from "svelte/store";
function toggleBurgerMenu () {
  const {subscribe, set, update} = writable(false);
  return {
    subscribe,
    reset: () => set(true),
    toggle: () => update(n => !n)
  };
}
export const burgerMenu = toggleBurgerMenu();
