# Svelte Project Template
A SPA project using SvelteJS, TailwindCSS, and Svelte-Material-UI.

## The stack consist of these main key libraries:
#### [Svelte](https://svelte.dev/)
    Extremely fast and lightweight front-end framework that does not use virtual DOM.
#### [Tailwind CSS](https://tailwindcss.com/)
    A utility-first CSS framework for rapidly building custom designs. Alternitive to Bootstrap.
#### [Svelte Material UI Components](https://github.com/hperrin/svelte-material-ui)
    A library of Svelte 3 Material UI components, based on the Material Design standards.
#### [Rollup](https://rollupjs.org/guide/en/)
    Alternative solution to Webpack or Bundle build by Rich Harris, the creator of Svelte.
#### [SASS](https://sass-lang.com/)
    A CSS compiler that allows you to write logic for CSS which reduces code repetitions.
#### [Svero Router](https://github.com/kazzkiq/svero)
    Svelte Routing library for Single Page Applications.
#### [Anime.js](https://animejs.com/)
    Animation library for make the app more fun to use.
## Build the project with the following NPM command in bash/powershell:
    - Prototyping and rapid-development : $ npm run build:dev
    - Production. Uglify and Minify     : $ npm run build:pro
## To run a local server with the builds:
    - Development : $ npm run dev
    - Production  : $ npm run pro
## Progress:
    - a work in progress
    - made further optimisations to reduce bloat and enables faster load times.
    - made the application PWA ready with service-worker.
    - further reading and playing with the @SMUI library.
    - added workbox instead of coding serviceworker by hand.
    - Lighthouse Audit scores of almost everything 100 points and PWA ready.
## Roadmap:
- [x] use Google Material icons
- [ ] responsive UI for mobile and web app  
- [ ] map integration
- [ ] search feature
- [ ] graph integration
- [ ] geolocation information
- [ ] profile page
- [ ] many more in the future...
