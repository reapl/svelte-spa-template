const gridConfig = require("./tailwind_plugins/tw-grid.config");
const grid       = require("./tailwind_plugins/tw-grid");
// const card       = require("./tailwind_plugins/tw-card");

module.exports = {
  // prefix: "tw-",
  theme: {
    extend: {
      colors: {
        backgroundColor: "#f6f5f7",
        primaryColor: "#5bd1d7",
        secondaryColor: "#248ea9",
        accentColor: "#556fb5"
      }
    },
    container: {
      center: true
    },
    extend: {}
  },
  variants: {},
  corePlugins: {
    preflight: !0
  },
  plugins: [
    grid(gridConfig), 
    // card
  ]
};
