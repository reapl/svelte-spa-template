const workboxConfig = {
  cacheId: "rocketboost",
  globStrict: true,
  globDirectory: "public/",
  globPatterns: [
    "**/*.{js,css,html,json,svg,png}"
  ],
  globIgnores: [
    "**/animated_rocket.svg",
    "**/_+sw.js",
    "**/favicon-{16x16,32x32,180x180,192x192,512x512}.png"
  ],
  swDest: "public/sw.js",
  runtimeCaching: [
    {
      urlPattern: /\.(?:svg|png|ico|jpg|jpeg)$/,
      handler: "CacheFirst",
      options: {
        cacheName: "Images",
        expiration: {
          maxEntries: 10,
          maxAgeSeconds: 5 * 60,
        },
        cacheableResponse: {
          statuses: [200]
        },
      }
    },
    {
      urlPattern: /.*fonts\.(?:googleapis|gstatic)\.com\/.*(?:css\?|icon\?|woff2)/,
      handler: "CacheFirst",
      options: {
        cacheName: "Google",
        expiration: {
          maxEntries: 3,
          maxAgeSeconds: 24 * 60 * 60,
        },
        cacheableResponse: {
          statuses: [0, 200]
        },
      }
    }
  ]
};

const workboxInject = {
  globStrict: true,
  globDirectory: "public/",
  globPatterns: ["**/*.{js,svg,png,ico,css,html,json}"],
  globIgnores: [
    "**/animated_rocket.svg",
    "**/_+sw.js",
    "**/favicon-{16x16,32x32,180x180,192x192,512x512}.png"
  ],
  swDest: "public/sw.js",
  swSrc: "workbox/src-sw.js"
};

export {workboxConfig, workboxInject};
