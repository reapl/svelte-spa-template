module.exports = {
  content: ["./src/**/*.svelte", "./public/**/*.html"],
  defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || [],
  // whitelistPatterns: [/^bg./, /^border./, /^text./]
};
